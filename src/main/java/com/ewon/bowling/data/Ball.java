package com.ewon.bowling.data;

import java.util.Optional;

import com.ewon.bowling.exception.InvalidPinException;

/**
 * A single Ball played in game, with pin point and link to next ball.
 * 
 * @author ddelbecq
 *
 */
public class Ball {
  private int pin;
  private Optional<Ball> nextBall = Optional.empty();

  /**
   * Construct a Ball with given pin points.
   * 
   * @param pin
   *          Number of pins down for that Ball.
   */
  public Ball(int pin) {
    setPin(pin);
  }

  /**
   * Get number of pins down for that Ball
   * 
   * @return number of pins down
   */
  public int getPin() {
    return this.pin;
  }

  private void setPin(int pin) throws InvalidPinException {
    if (pin < 0 || pin > 10) {
      throw new InvalidPinException();
    }
    this.pin = pin;
  }

  /**
   * Get a link to the next Ball
   * 
   * @return {@link Optional} representing next ball
   */
  public Optional<Ball> getNextBall() {
    return this.nextBall;
  }

  /**
   * Set the {@link Optional} representing next Ball.
   * 
   * @return {@link Optional} representing next ball. Can be empty, can not be
   *         null
   */
  public void setNextBall(Optional<Ball> nextBall) {
    assert nextBall != null;
    this.nextBall = nextBall;
  }

  /**
   * Set the next Ball. This is a the same as calling
   * {@link #setNextBall(Optional)} after wrapping the parameter with
   * {@link Optional#ofNullable(Object)}
   * 
   * @return {Next ball. Can be can be null.
   */
  public void setNextBall(Ball nextBall) {
    this.nextBall = Optional.ofNullable(nextBall);
  }

}
