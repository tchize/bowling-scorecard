package com.ewon.bowling.data;

import java.util.Optional;

import com.ewon.bowling.exception.InvalidBallException;
import com.ewon.bowling.exception.InvalidPinException;

/**
 * Bowling Frame. This represents the one, two or three shoot (last frame) that
 * player did during that Frame. Makes points calculation for that Frame.
 * Setting frame balls must be done in correct order. Starting from ball 1 to
 * ball 2 (or 3 under certain conditions for last ball)
 * 
 * @see <a href=
 *      "https://en.wikipedia.org/wiki/Ten-pin_bowling#Scoring">Wikipedia
 *      bownling rules</a>
 * @author ddelbecq
 *
 */
public class Frame {

  private static final Integer TOTAL_PINS = 10;
  @SuppressWarnings("unchecked")
  private Optional<Ball>[] balls = new Optional[] {
      Optional.empty(), Optional.empty(), Optional.empty()
  };
  private boolean lastFrame;

  /**
   * Set the pin points for specific ball
   * 
   * @param ballNumber
   *          the ball number to set pin points about. Value between 1 and 3.
   *          Value 3 is only allowed for last frame. Value 2 is only allowed if
   *          it not a strike. If current frame is last frame, and it's a
   *          strike, value 2 and 3 are allowed
   * @param ball
   *          the ball value
   * @throws InvalidPinException
   *           if pin point of given ball is too much for current frame
   * @throws InvalidBallException
   *           if trying to set invalid ball index (see ballNumber description)
   */
  public void setBallPins(int ballNumber, Ball ball) {
    checkBallNumber(ballNumber);
    if (getPinsUp() < ball.getPin()) {
      throw new InvalidPinException(
          "Only " + getPinsUp() + " pins left");
    }
    this.balls[ballNumber - 1] = Optional.of(ball);
  }

  /**
   * Check if this frame is a strike. This only check for a Strike in first
   * Ball. On last frame, you could have a Strike on second or third ball, but
   * it's not relevant to this method.
   * 
   * @return true if strike
   */
  public boolean isStrike() {
    return this.balls[0].map(Ball::getPin).equals(Optional.of(TOTAL_PINS));
  }

  /**
   * Check if this frame is a spare. This only checks for a spare based on first
   * 2 balls. In last frame you could have a spare based on second and third
   * balls, but it's not relevant to this method.
   * 
   * @return true if spare
   */
  public boolean isSpare() {
    return !isStrike() && TOTAL_PINS.equals(
        this.balls[0].map(Ball::getPin).orElse(0) +
            this.balls[1].map(Ball::getPin).orElse(0));
  }

  /**
   * Get current total points of this frame, including strike bonus
   * 
   * @return current calculation of total point of this frame
   */
  public int getPoints() {
    if (isStrike()){
      // ball 0 + next ball + next next ball
      return this.balls[0].map(Ball::getPin).orElse(0) +
          this.balls[0].flatMap(Ball::getNextBall).map(Ball::getPin).orElse(0) +
          this.balls[0].flatMap(Ball::getNextBall).flatMap(Ball::getNextBall).map(Ball::getPin).orElse(0);
    }else if (isSpare()){
      // ball 0 + ball 1 + next ball
      return this.balls[0].map(Ball::getPin).orElse(0) +
          this.balls[1].map(Ball::getPin).orElse(0) +
          this.balls[1].flatMap(Ball::getNextBall).map(Ball::getPin).orElse(0);
    } else {
      // ball 0 + ball 1 + next ball
      return this.balls[0].map(Ball::getPin).orElse(0) +
          this.balls[1].map(Ball::getPin).orElse(0);
    }
  }

  /**
   * Check if this frame has all required balls collected
   * 
   * @return true if all balls of this frame collected, false otherwise
   */
  public boolean isComplete() {
    if (lastFrame){
      if (isStrike() || isSpare()){
        return this.balls[1].isPresent() && this.balls[2].isPresent();
      } else {
        return this.balls[1].isPresent();
      }
    } else {
      return isStrike() || this.balls[1].isPresent();
    }
  }

  /**
   * Check if this frame is the last frame
   * 
   * @return true if last frame
   */
  public boolean isLastFrame() {
    return this.lastFrame;
  }

  /**
   * Set the lastFrame flag. Last frame has a slightly different calculation
   * mecanism
   * 
   * @param lastFrame
   *          value of last frame flag.
   */
  public void setLastFrame(boolean lastFrame) {
    this.lastFrame = lastFrame;
  }

  /**
   * Return given ball pin down.
   * 
   * @param ballNumber
   *          the frame ball number to get
   * @return empty optional if ball not yet thrown, an optional with value
   *         between 0 and 10 if it was thrown.
   * @throws ArrayIndexOutOfBoundsException
   *           if ball number is not in range 1 - 3
   */
  public Optional<Integer> getBallPins(int ballNumber) {
    return this.balls[ballNumber - 1].map(Ball::getPin);
  }

  /**
   * Get the number of pins still up in that Frame.
   * 
   * @return
   */
  public int getPinsUp() {
    if (this.lastFrame) {
      int val = ((2 * TOTAL_PINS)
          - this.balls[0].map(Ball::getPin).orElse(0)
          - this.balls[1].map(Ball::getPin).orElse(0)) % TOTAL_PINS;
      return val == 0 ? 10 : val; // in last frame, all pins down brings new set
                                  // of pins
    }
    return TOTAL_PINS - this.balls[0].map(Ball::getPin).orElse(0) - this.balls[1].map(Ball::getPin).orElse(0);
  }

  private void checkBallNumber(int ballNumber) {
    if (this.balls[ballNumber - 1].isPresent()) {
      throw new InvalidBallException("Ball " + ballNumber + " already recorded");
    }
    if (ballNumber == 3 && (!this.lastFrame || !(isStrike() || isSpare()))) {
      throw new InvalidBallException("Ball 3 only allowed in last frame with strike or spare");
    }
    if (ballNumber == 2) {
      if (!this.balls[0].isPresent()) {
        throw new InvalidBallException("Set ball 1 first");
      }
      if (isStrike()) {
        if (!this.lastFrame) {
          throw new InvalidBallException("No second ball after a strike");
        }
      }
    }
  }
}
