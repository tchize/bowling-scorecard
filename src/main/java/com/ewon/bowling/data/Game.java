package com.ewon.bowling.data;

import java.util.Arrays;

/**
 * A bowling game, made of 10 Frames. Use this class by adding balls, one at a
 * time, to the game. They will be put to the correct frames.
 * 
 * @author ddelbecq
 *
 */
public class Game {

  public static final int GAME_FRAMES = 10;
  private Frame[] frames;
  private int currentFrame;
  private Ball previousBall;
  private int currentBallIndex = 1;

  /**
   * Create a new game.
   */
  public Game() {
    this.frames = new Frame[GAME_FRAMES];
    for (int i = 0; i < GAME_FRAMES; i++) {
      this.frames[i] = new Frame();
    }
    this.frames[GAME_FRAMES - 1].setLastFrame(true);
  }

  /**
   * Get the total score of that game.
   * 
   * @return total score of game
   */
  public int getPoints() {
    return Arrays.stream(frames).mapToInt(Frame::getPoints).sum();
  }

  /**
   * Get a given frame from the game.
   * 
   * @return the give frame
   * @throws ArrayIndexOutOfBoundsException
   *           of index is not in range [0-10[
   */
  public Frame getFrame(int index) {
    return this.frames[index];
  }

  /**
   * Add a ball result to the current game
   * 
   * @param pins
   */
  public void addBall(int pins) {
    Ball b = new Ball(pins);
    if (previousBall != null) {
      this.previousBall.setNextBall(b);
    }
    this.previousBall = b;
    this.frames[currentFrame].setBallPins(currentBallIndex, b);
    currentBallIndex++;
    if (this.frames[currentFrame].isComplete()) {
      this.currentFrame++;
      this.currentBallIndex = 1;
    }
  }

  /**
   * Check if game is finished.
   * 
   * @return true if last ball of game was send.
   */
  public boolean isComplete() {
    return Arrays.stream(this.frames).allMatch(Frame::isComplete);
  }

  /**
   * Get the number of pins currently up in the game
   * 
   * @return number of pins up in current play frame.
   */
  public int getPinsUp() {
    return this.frames[currentFrame].getPinsUp();
  }
}
