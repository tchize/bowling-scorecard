package com.ewon.bowling.view;

import java.util.Arrays;

import com.ewon.bowling.data.Frame;

/**
 * Class used to generate the console view of a Game Frame
 * 
 * @author ddelbecq
 *
 */
public class FrameView {

  private static final String MARKER_EMPTY = " ";
  private static final String MARKER_SPARE = "/";
  private static final String MARKER_STRIKE = "X";
  private static final String MARKER_ZERO = "-";
  private static final String[] SCORE_FORMAT = new String[] {
      "+-----+",
      "|%1$1s|%2$1s|%3$1s|",
      "|  %4$3s|",
      "+-----+"
  };
  public static final int VIEW_ROWS = SCORE_FORMAT.length;


  private static String toBallPoint(Integer value) {
    return value == 10 ? MARKER_STRIKE : (value == 0 ? MARKER_ZERO : value.toString());
  }

  private Frame frame;

  /**
   * Create a view based on given {@link Frame}
   * 
   * @param frame
   *          the frame to display
   */
  public FrameView(Frame frame) {
    this.frame = frame;
  }
  
  /**
   * Get the lines of String used to represent linked Frame as board column. The
   * total score being an accumulation of other frame scores, provide in
   * parameter the score before that frame.
   * 
   * @param baseScore
   *          the score before that frame
   * @return an array of String representing the {@link Frame} column
   */
  public String[] getScoreLines(int baseScore){
    Object[] formatArgs;
    if (this.frame.isLastFrame()){
      formatArgs = new Object[] {
          getFirstBall(),
          getSecondBall(),
          getThirdBall(),
          getTotalScore(baseScore)
      };
    } else {
      formatArgs = new Object[] {
          MARKER_EMPTY,
          getFirstBall(),
          getSecondBall(),
          getTotalScore(baseScore)
      };
    }
    return Arrays.stream(SCORE_FORMAT).map((s) -> String.format(s, formatArgs)).toArray(String[]::new);
  }

  private String getTotalScore(int baseScore) {
    if (this.frame.getBallPins(1).isPresent()) {
      return Integer.toString(this.frame.getPoints() + baseScore);
    } else {
      return "";
    }
  }

  private String getFirstBall() {
    if (this.frame.isStrike()) {
      return MARKER_STRIKE;
    } else {
      return this.frame.getBallPins(1).map(FrameView::toBallPoint).orElse(MARKER_EMPTY);
    }
  }

  private String getSecondBall() {
    if (this.frame.isStrike() && !this.frame.isLastFrame()) {
      return MARKER_EMPTY;
    } else if (this.frame.isSpare()) {
      return MARKER_SPARE;
    } else {
      return this.frame.getBallPins(2).map(FrameView::toBallPoint).orElse(MARKER_EMPTY);
    }
  }

  private String getThirdBall() {
    return this.frame.getBallPins(3).map(FrameView::toBallPoint).orElse(MARKER_EMPTY);
  }

}
