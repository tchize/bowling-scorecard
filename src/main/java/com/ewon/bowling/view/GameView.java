package com.ewon.bowling.view;

import java.util.Arrays;
import java.util.stream.IntStream;

import com.ewon.bowling.data.Game;

/**
 * Represent the score card view of a Game
 * 
 * @author ddelbecq
 *
 */
public class GameView {
  private FrameView[] frameViews;
  private Game game;

  /**
   * Create a score card representation of the given game.
   * 
   * @param game
   *          the {@link Game} to display
   */
  public GameView(Game game) {
    this.game = game;
    this.frameViews = IntStream.range(0, Game.GAME_FRAMES)
        .mapToObj(this.game::getFrame)
        .map(FrameView::new)
        .toArray(FrameView[]::new);
  }

  /**
   * Get the current score card of game represented by this view
   * 
   * @return an array of String representing score card, one for each line of
   *         the card.
   */
  public String[] getScoreCard() {
    StringBuilder[] response = new StringBuilder[FrameView.VIEW_ROWS];
    for (int i = 0; i< response.length;i++){
        response[i] = new StringBuilder();
    }
    int baseScore = 0;
    for (int i = 0; i<frameViews.length;i++){
      String[] col = frameViews[i].getScoreLines(baseScore);
      baseScore += this.game.getFrame(i).getPoints();
      for (int j = 0; j < col.length; j++) {
        response[j].append(col[j]);
      }
    }
    return Arrays.stream(response).map(Object::toString).toArray(String[]::new);
  }
}
