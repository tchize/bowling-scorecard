package com.ewon.bowling.exception;

public class InvalidBallException extends RuntimeException {

  /**
   * Exception thrown when a ball index is not allowed in a frame
   */
  private static final long serialVersionUID = -7660759497180983636L;

  public InvalidBallException() {
    super();
  }

  public InvalidBallException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public InvalidBallException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidBallException(String message) {
    super(message);
  }

  public InvalidBallException(Throwable cause) {
    super(cause);
  }

}
