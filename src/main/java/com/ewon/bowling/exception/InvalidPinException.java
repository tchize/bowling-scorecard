package com.ewon.bowling.exception;

public class InvalidPinException extends RuntimeException {

  /**
   * Exception thrown when Pin value is out of range
   */
  private static final long serialVersionUID = -7660759497180983636L;

  public InvalidPinException() {
    super();
  }

  public InvalidPinException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public InvalidPinException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidPinException(String message) {
    super(message);
  }

  public InvalidPinException(Throwable cause) {
    super(cause);
  }

}
