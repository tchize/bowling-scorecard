package com.ewon.bowling;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.ewon.bowling.data.Game;
import com.ewon.bowling.exception.InvalidPinException;
import com.ewon.bowling.view.GameView;

/**
 * Bowling game
 */
public class Bowling {
  private Game game;
  private GameView gameView;

  public Bowling() {
    game = new Game();
    gameView = new GameView(game);
  }

  public void run() {
    try (Scanner scanner = new Scanner(System.in)) {

      while (!game.isComplete()) {
        printScoreCard();
        System.out.print("How many pins down? ");
        try {
          int pins = scanner.nextInt();
          game.addBall(pins);
        } catch (InvalidPinException e) {
          System.out.println("Invalid number of pins. Only " + this.game.getPinsUp() + " left ");
        } catch (InputMismatchException e) {
          System.out.println("Invalid value.");
        }
        scanner.nextLine();
      }
      printScoreCard();
    }
  }

  private void printScoreCard() {
    for (String line : gameView.getScoreCard()) {
      System.out.println(line);
    }
  }

  public static void main(String[] args) {
    new Bowling().run();
  }
}
