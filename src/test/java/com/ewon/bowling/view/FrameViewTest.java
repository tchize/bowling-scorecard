package com.ewon.bowling.view;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ewon.bowling.data.Ball;
import com.ewon.bowling.data.Frame;

public class FrameViewTest {

  @Test
  public void getScoreLineStrike() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(10));
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(0), new String[] {
        "+-----+",
        "| |X| |",
        "|   10|",
        "+-----+"
    });
  }

  @Test
  public void getScoreLineStrikeLastFrame() {
    Frame f = new Frame();
    f.setLastFrame(true);
    f.setBallPins(1, new Ball(10));
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(92), new String[] {
        "+-----+",
        "|X| | |",
        "|  102|",
        "+-----+"
    });
  }

  @Test
  public void getFullScoreLineStrikeLastFrame() {
    Frame f = new Frame();
    f.setLastFrame(true);
    Ball ball1 = new Ball(10);
    Ball ball2 = new Ball(8);
    Ball ball3 = new Ball(2);
    ball1.setNextBall(ball2);
    ball2.setNextBall(ball3);

    f.setBallPins(1, ball1);
    f.setBallPins(2, ball2);
    f.setBallPins(3, ball3);
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(92), new String[] {
        "+-----+",
        "|X|8|2|",
        "|  112|",
        "+-----+"
    });
  }

  @Test
  public void getScoreLineStrikeWithBase() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(10));
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(152), new String[] {
        "+-----+",
        "| |X| |",
        "|  162|",
        "+-----+"
    });
  }

  @Test
  public void getScoreZeroPins() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(0));
    f.setBallPins(2, new Ball(5));
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(0), new String[] {
        "+-----+",
        "| |-|5|",
        "|    5|",
        "+-----+"
    });
  }

  @Test
  public void getSimpleScore() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(2));
    f.setBallPins(2, new Ball(5));
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(0), new String[] {
        "+-----+",
        "| |2|5|",
        "|    7|",
        "+-----+"
    });
  }

  @Test
  public void getNoBallScore() {
    Frame f = new Frame();
    FrameView fv = new FrameView(f);
    Assert.assertEquals(fv.getScoreLines(0), new String[] {
        "+-----+",
        "| | | |",
        "|     |",
        "+-----+"
    });
  }
}
