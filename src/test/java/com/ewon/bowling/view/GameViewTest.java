package com.ewon.bowling.view;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ewon.bowling.data.Game;

public class GameViewTest {

  @Test
  public void getScoreCard() {
    Game g = new Game();
    g.addBall(10);
    g.addBall(7);
    g.addBall(3);
    g.addBall(9);
    g.addBall(0);
    g.addBall(10);
    g.addBall(0);
    g.addBall(8);
    g.addBall(8);
    g.addBall(2);
    g.addBall(0);
    g.addBall(6);
    g.addBall(10);
    g.addBall(10);
    g.addBall(10);
    g.addBall(8);
    g.addBall(1);
    GameView gv = new GameView(g);
    Assert.assertEquals(gv.getScoreCard(), new String[] {
        "+-----++-----++-----++-----++-----++-----++-----++-----++-----++-----+",
        "| |X| || |7|/|| |9|-|| |X| || |-|8|| |8|/|| |-|6|| |X| || |X| ||X|8|1|",
        "|   20||   39||   48||   66||   74||   84||   90||  120||  148||  167|",
        "+-----++-----++-----++-----++-----++-----++-----++-----++-----++-----+"
    });
  }
}
