package com.ewon.bowling.data;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ewon.bowling.exception.InvalidPinException;

public class GameTest {

  @Test(expectedExceptions = InvalidPinException.class)
  public void tooManyPins() {
    Game g = new Game();
    g.addBall(8);
    g.addBall(3);
  }

  @Test
  public void retryAfterTooManyPins() {
    Game g = new Game();
    g.addBall(8);
    try {
      g.addBall(3);
    } catch (InvalidPinException e) {
    }
    g.addBall(2);

  }

  @Test
  public void getPinUps() {
    Game g = new Game();
    Assert.assertEquals(g.getPinsUp(), 10);
    g.addBall(8);
    Assert.assertEquals(g.getPinsUp(), 2);
    g.addBall(1);
    Assert.assertEquals(g.getPinsUp(), 10);
    g.addBall(1);
    Assert.assertEquals(g.getPinsUp(), 9);
  }

  @Test(expectedExceptions = InvalidPinException.class)
  public void tooNegativePins() {
    Game g = new Game();
    g.addBall(-1);
  }

  @Test(expectedExceptions = InvalidPinException.class)
  public void tooTooHighPin() {
    Game g = new Game();
    g.addBall(11);
  }

  @Test
  public void incompleteGame() {
    Game g = new Game();
    g.addBall(8);
    g.addBall(2);

    g.addBall(5);
    g.addBall(4);
    assertFalse(g.isComplete());

  }

  @Test
  public void simpleGame() {
    Game g = new Game();
    g.addBall(8);
    g.addBall(2);

    g.addBall(5);
    g.addBall(4);

    g.addBall(9);
    g.addBall(0);

    g.addBall(10);

    g.addBall(10);

    g.addBall(5);
    g.addBall(5);

    g.addBall(5);
    g.addBall(3);

    g.addBall(6);
    g.addBall(3);

    g.addBall(9);
    g.addBall(1);

    g.addBall(9);
    g.addBall(1);
    g.addBall(10);

    assertEquals(g.getPoints(), 149);
    assertTrue(g.isComplete());

  }

  @Test
  public void perfectGame() {
    Game g = new Game();
    for (int i = 0; i < 12; i++) {
      g.addBall(10);
    }

    assertEquals(g.getPoints(), 300);
    assertTrue(g.isComplete());

  }
}
