package com.ewon.bowling.data;

import org.testng.annotations.Test;

import com.ewon.bowling.exception.InvalidPinException;

public class BallTest {

  @Test(expectedExceptions = InvalidPinException.class)
  public void setNegativePin() {
    new Ball(-1);
  }

  @Test(expectedExceptions = InvalidPinException.class)
  public void setTooBigPin() {
    new Ball(11);
  }
}
