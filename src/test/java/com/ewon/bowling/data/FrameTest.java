package com.ewon.bowling.data;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ewon.bowling.exception.InvalidBallException;
import com.ewon.bowling.exception.InvalidPinException;

public class FrameTest {
  @Test
  public void testStrike() {
    Frame f = new Frame();
    Ball ball1 = new Ball(10);
    f.setBallPins(1, ball1);
    assertTrue(f.isStrike(), "Should be a strike when putting down all pins in first ball");
    assertEquals(10, f.getPoints());
    Ball ball2 = new Ball(3);
    Ball ball3 = new Ball(6);
    ball1.setNextBall(ball2);
    ball2.setNextBall(ball3);
    assertEquals(19, f.getPoints());

  }

  @Test
  public void testNonStrike() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(9));
    assertFalse(f.isStrike(), "Should not be a strike when putting down less than pins in first ball");
  }

  @Test
  public void testNonSpare() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(9));
    assertFalse(f.isSpare(), "Should not be a spare when no info given on second ball");
    f.setBallPins(2, new Ball(0));
    assertFalse(f.isSpare(), "Should not be a spare when not all pins are down after second ball");
  }
  
  @Test
  public void testSpare() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(5));
    f.setBallPins(2, new Ball(5));
    assertFalse(f.isStrike(), "Should not be a strike when putting down less than pins in first ball");
    assertTrue(f.isSpare(), "Should be a spare when putting down all pins in two balls");
  }
  
  @Test
  public void testStrikeIsNotSpare() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(10));
    assertTrue(f.isStrike(), "Should be a strike when putting down pins on first ball");
    assertFalse(f.isSpare(), "A strike is not a spare");
  }

  @Test
  public void testRhinoPoints() {

    Frame f1 = new Frame();
    Frame f2 = new Frame();
    Frame f3 = new Frame();
    Ball ball1_1 = new Ball(10);
    Ball ball2_1 = new Ball(10);
    Ball ball3_1 = new Ball(9);
    Ball ball3_2 = new Ball(0);
    ball1_1.setNextBall(ball2_1);
    ball2_1.setNextBall(ball3_1);
    ball3_1.setNextBall(ball3_2);
    f1.setBallPins(1, ball1_1);
    f2.setBallPins(1, ball2_1);
    f3.setBallPins(1, ball3_1);
    f3.setBallPins(2, ball3_2);
    assertEquals(f1.getPoints(), 29);
    assertEquals(f2.getPoints(), 19);
    assertEquals(f3.getPoints(), 9);
  }

  @Test
  public void testSparePoints() {
    Frame f = new Frame();
    Ball ball1_1 = new Ball(7);
    f.setBallPins(1, ball1_1);
    Ball ball1_2 = new Ball(3);
    f.setBallPins(2, ball1_2);
    assertTrue(f.isSpare(), "Should be a spare when putting down all pins in two balls");
    Ball ball2_1 = new Ball(4);
    ball1_2.setNextBall(ball2_1);
    assertEquals(f.getPoints(), 14);
  }
  
  @Test(expectedExceptions = InvalidPinException.class)
  public void testPinsOverflow() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(8));
    f.setBallPins(2, new Ball(4));
  }

  @Test(expectedExceptions = InvalidPinException.class)
  public void testPinsOverflowLastFrame() {
    Frame f = new Frame();
    f.setLastFrame(true);
    f.setBallPins(1, new Ball(10));
    f.setBallPins(2, new Ball(7));
    f.setBallPins(3, new Ball(4));
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testBallOverflow() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(10));
    f.setBallPins(2, new Ball(0));
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testMissingOverflow() {
    Frame f = new Frame();
    f.setBallPins(2, new Ball(5));
  }

  @Test
  public void testFrameComplete() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(5));
    assertFalse(f.isComplete());
    f.setBallPins(2, new Ball(3));
    assertTrue(f.isComplete());
  }

  @Test
  public void testLastFrameComplete() {
    Frame f = new Frame();
    f.setLastFrame(true);
    Ball ball1 = new Ball(10);
    f.setBallPins(1, ball1);
    assertFalse(f.isComplete());
    Ball ball2 = new Ball(3);
    ball1.setNextBall(ball2);
    f.setBallPins(2, ball2);
    assertFalse(f.isComplete());
    Ball ball3 = new Ball(4);
    ball2.setNextBall(ball3);
    f.setBallPins(3, ball3);
    assertTrue(f.isComplete());
    assertEquals(f.getPoints(), 17);
  }

  @Test
  public void testGetPinsUp() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(3));
    Assert.assertEquals(f.getPinsUp(), 7);
    f.setBallPins(2, new Ball(7));
    Assert.assertEquals(f.getPinsUp(), 0);
  }

  @Test
  public void testGetPinsUpLastFrame() {
    Frame f = new Frame();
    f.setLastFrame(true);
    f.setBallPins(1, new Ball(3));
    Assert.assertEquals(f.getPinsUp(), 7);
    f.setBallPins(2, new Ball(7));
    Assert.assertEquals(f.getPinsUp(), 10);
  }

  @Test
  public void testGetPinsUpLastFrameStrike() {
    Frame f = new Frame();
    f.setLastFrame(true);
    f.setBallPins(1, new Ball(10));
    Assert.assertEquals(f.getPinsUp(), 10);
    f.setBallPins(2, new Ball(7));
    Assert.assertEquals(f.getPinsUp(), 3);
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testFrameStartWithBall2() {
    Frame f = new Frame();
    f.setBallPins(2, new Ball(5));
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testFrameStartWithBall3() {
    Frame f = new Frame();
    f.setLastFrame(true);
    f.setBallPins(3, new Ball(5));
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testFrameTwiceBall1() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(5));
    f.setBallPins(1, new Ball(5));
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testFrameTwiceBall2() {
    Frame f = new Frame();
    f.setBallPins(1, new Ball(2));
    f.setBallPins(2, new Ball(3));
    f.setBallPins(2, new Ball(3));
  }

  @Test(expectedExceptions = InvalidBallException.class)
  public void testFrameTwiceBall3() {
    Frame f = new Frame();
    f.setLastFrame(true);
    f.setBallPins(1, new Ball(10));
    f.setBallPins(2, new Ball(2));
    f.setBallPins(3, new Ball(5));
    f.setBallPins(3, new Ball(5));
  }
}
